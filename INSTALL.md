# Local Installation
This guide will support you with the local setup of the _BYOD Launcher_ application for development purposes. It is **not** suitable for production environments.

Before you continue with the installation, it is assumed, you already cloned this repository to you local development machine. It might be easier to collaborate for you and your team mates, if somebody of your team creates a **fork** of this repository and all of you make your own clone of that fork instead of cloning this repository directly.

[[_TOC_]]

## .NET Core SDK
You need the .NET Core SDK in version 3.1 or higher. You might verify this with the command `dotnet --info`.

In case you ned to install .NET Core SDK, head over to [https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download).

## Node Package Manager (npm)
For building the frontends components, you need the command-line tool `npm`. If you already worked with frontend frameworks like _Angular_, _React_, _Vue_ or developed backend services with _Node.js_, you probably have `npm` installed already.

To verify whether you have _Node.js_ and _npm_ installed, use the `node -v` and `npm -v` command respectively.

Head over to [https://nodejs.org/](https://nodejs.org/) to install _npm_ along with _Node.js_.

## Database
For the data storage you need a local running _MariaDB_ database server. Depending on your preferences, you may install such a database server on your local machine or you might run it inside a Docker container.

### Install Database Server
#### Variant A: Local Database Server
To install a local _MariaDB_ database server, head over to [https://mariadb.org/download/](https://mariadb.org/download/) to download the installer for your operating system. Run the installer and start the database server.

Using a local installed database server, _MariaDB_ is probably running on the default port `3306`. You need to change the corresponding `Port` directive of the connection string in [appsettings.Development.json](appsettings.Development.json).

#### Variant B: Docker Container
To run _MariaDB_ in a Docker container, you need a running instance of Docker and Docker Compose. The easiest way to achieve this is by running the [installer for Docker Desktop](https://docs.docker.com/desktop/#download-and-install) (Windows, macOS).

To fire up a Docker container which runs _MariaDb_ you can point your console to the [Docker](Docker) directory of this repository. Inside you'll find a file named [docker-compose.yaml](Docker/docker-compose.yaml). Execute `docker-compose up` to run a docker-compose database service named _db_. By appending the `-d` flag, the process will run in the background. You may then stop the running Docker container(s) using the `docker-compose down` command.

```shell
# Change into the Docker directory.
cd Docker/

# Start the database container (in background).
docker-compose up -d
```

Note, that the _MariaDb_ database server inside the Docker container will be reachable locally on port `13306`.

### Create Database and User
**If you choose to run a containerized version of MariaDB (Docker Container), you may skip this step since the database and user will be created when the container starts.**
 
To create the initial, empty database and a database user with access rights for this database, execute the following SQL statements:

```sql
-- Create database named "byodlauncher"
CREATE DATABASE byodlauncher;

-- Create user "user" with password "password" with access on the "byodlauncher" database
GRANT ALL PRIVILEGES ON byodlauncher.* TO 'user'@'%' identified by 'password';

-- Reload privileges
FLUSH PRIVILEGES;
```

### Import Example Data
You might want to create the required tables for the application and insert some sample data. For this purpose, you might insert the content of the file [sampleData.sql](sampleData.sql) into your previously created database.

Depending on your experience and needs, you might want to use the commandline or a graphical database client to import. So, either use your favorite SQL Client or the `mysql` commandline tool to connect to the running database server and insert/execute the contents of the [sampleData.sql](sampleData.sql) file.

#### Graphical Database Client

Recommended graphical clients which work with _MariaDB_ are [HeidiSQL](https://www.heidisql.com/) (which comes bundled with the Windows version of the local _MariaDB_ database server!) or most graphical _MySQL_ clients like [phpMyAdmin](https://www.phpmyadmin.net/) (which you might already have installed with your local [XAMPP](https://www.apachefriends.org) application). Just use them to connect to the database server.

#### Commandline Interface

##### Variant A: Local Database Server
Running a local _MariaDB_ server, use the commandline to navigate to this projects root directory and use the following command to import the sample data into the local database server:

```shell
# Connect with "user" and "password" to database "byodlauncher" to import the contents of "sampleData.sql"
mysql -u user -p byodlauncher < sampleData.sql

# Enter password "password" and confirm with ENTER
```

##### Variant B: Docker Container
Running a _MariaDB_ server inside a docker container, use the commandline to navigate to the [Docker](Docker) directory and use the following commands to connect to the database server:

```shell
# Start an interactive shell inside the "db" docker container
docker-compose exec db /bin/bash

# Navigate to the /root/ directory (where the sampleData.sql) file from your host machine is linked to.
cd /root/

# Connect wither "user" and "password" to database "byodlauncher" to import the contents of "sampleData.sql"
mysql -u user -p byodlauncher < sampleData.sql

# Enter password "password" and confirm with ENTER
```

## Configuration
The _BYOD Launcher_ application has two files, where configuration is stores (as most .NET Core applications do). The [appsettings.json](appsettings.json) file contains all relevant configuration items **for the production environment**. Even in case you won't need some configuration items in production environment, you must create a corresponding entry in this file, if you'd like to use it in any other environment.

The `appsettings.Development.json` file contains configuration **overwrites** for any items which are introduced in [appsettings.json](appsettings.json). Here you want to make some changes before/while developing the application. To start, you need to copy or rename the corresponding file with the `.TEMPLATE` suffix ([appsettings.Development.json.TEMPLATE](appsettings.Development.json.TEMPLATE)) to a version *without* the `.TEMPLATE` suffix.

> The suffix .TEMPLATE was introduced to prevent git merge conflicts: Since the contents of this configuration files fit your local development environment, you usually don't want to share it with your team mates. The actual `appsettings.Development.json` file is therefore excluded from tracking in git (it is listed in the `.gitignore` file).

After you successfully introduced your very own `appsettings.Development.json` file, you'll provide/verify at least two configuration items.

### Connection String
The connection string at configuration path `ConnectionString.MariaDb` contains the relevant directives for the local database connection. Verify, that the various directives match your actual environment. Default values are:

- Server = `localhost`
- Database = `byodlauncher`
- Port = `13306` --> you need to change this to `3306` if you're running the local database server (variant A)
- User = `user`
- Password = `password`

### Absolute Path for File Uploads
Users of the application might upload some files (images) while creating new tutorial steps. The configuration item at path `FileUpload.FilesystemAbsolutePath` should contain the absolute path to a (writable) directory where these uploads should be persisted.

For your local development environment it is recommended to create a new and empty directory at root level of this project for these uploaded files (i.e. `Uploads`). Copy the absolute path of this directory and paste it at the corresponding place in the configuration file (i.e. replace `<DEVELOPMENT-ENVIRONMENT-ABSOLUTE-PATH-HERE>`). Don't forget to add the directory to your [.gitignore](.gitignore) file since you probably don't want to synchronize these files with anybody (there's already an entry in .gitignore for a directory called `Uploads` - so you don't need to add anything in case you choose the same name for your uploads directory).

## Running the Application
Finally, you might run the application. It won't be sufficient to just hit the play button in you IDE although. Since the project consists of two quite distinct parts, you need to start them individually.

### Frontend

#### Install the Dependencies
Before you run the frontend for the first time, you need to install the dependencies. Execute the following commands to do exactly that:

```shell
# Change into the wwwroot directory
cd wwwroot

# Install the dependencies
npm install
```
#### Run the Frontend
To run the frontend, use the commandline to navigate to the [wwwroot](wwwroot) directory and execute the following command to run the frontend:

```shell
# Run the frontend
npm run serve
```

For the very first launch, this command might take a while to download and install all dependencies. Don't worry although - for subsequent launches, it will be much faster :-)

You might want to try if the frontend is reachable already? Just click on the link in the commandline or open [your favorite browser](https://brave.com/de/) and enter the url [http://localhost:8080](http://localhost:8080) manually.

### Run the Backend
Since the backend is just a plain ASP.NET Core application, you finally might want to hit the play button in your IDE. In case you liked working with the commandline, there's of course a command for that, too:

```shell
# Run the backend using the dotnet commandline tool
dotnet run --launch-profile ByodLauncher
```

### Verify Running Application
Depending on the way to start the backend, your local application runs on different ports. For HTTPS connections, this may be ports `5001` or `44369`. For unsecured HTTP connections, this may be ports `5000` or `43847`.

For the frontend, there's a configuration item named `VUE_APP_API_HOST` in the file [wwwroot/.env.development](wwwroot/.env.development). The default value in this configuration file is set to `http://localhost:5000`.

Since the application is configured to accept network requests only from the same origin, the ports of this configuration item and the running application must match. You can either update the configuration in [wwwroot/.enf.development](wwwroot/.env.development) or the ports in [Properties/launchSettings.json](Properties/launchSettings.json). For any changes in these configuration files to take effect, the respective part of the application (either backend or frontend) must be restarted.

To finally verify, that everything works as expected, you may now simulate an authentication request and create a new session. By doing so, you may confirm for the database, the backend and the frontend as well as the communication channels between these parts are fully functional.

1. Visit [https://m150.gibz-informatik.ch](https://m150.gibz-informatik.ch)
2. Fill in the form. For a valid authentication request
    - `User ID` must be a valid [GUID](https://www.guidgenerator.com)
    - `First Name` and `Last Name` are for you to choose
    - The `Target URI` must point to the path `/api/authentication` on your (running) local application. Example: `http://localhost:5000/api/authentication`
    - `HTTP Method` for the request must be `POST`
    
The submission of this form simulates an authentication request originating from the [GIBZ Portal](https://portal.gibz.ch). "Everything" works fine if you are redirected to your local application. You may want to create a new session, close the browser and re-authenticated as described above. Since the previously created session will be assigned to the authenticated user, upon later, successfull authentication requests using **the same** user id, all these sessions should be available for editing.