import Vue from 'vue'
import Vuex from 'vuex'
import director from '@/store/modules/director'
import session from '@/store/modules/session'
import stage from "@/store/modules/stage";
import stageTarget from "@/store/modules/stageTarget";
import target from "@/store/modules/target";
import tutorialTarget from "@/store/modules/tutorialTarget";
import tutorialStep from "@/store/modules/tutorialStep";
import attendSession from "@/store/modules/attendSession";
import orchestrateSession from "@/store/modules/orchestrateSession";
import signalR from "@/store/modules/signalR";
import createSignalRPlugin from "@/store/plugin/signalR";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoading: false,
        loadingCounter: 0,
    },
    getters: {
        isLoading: (state) => {
            return state.loadingCounter > 0;
        }
    },
    actions: {
        START_LOADING({commit}) {
            commit('SET_LOADING_STATE', true);
        },

        FINISH_LOADING({commit}) {
            commit('SET_LOADING_STATE', false);
        },

        SET_LOADING({commit}, isLoading: boolean) {
            commit('SET_LOADING_STATE', isLoading);
        }

    },
    mutations: {
        SET_LOADING_STATE(state, newLoadingStage: boolean) {
            if (newLoadingStage) {
                state.loadingCounter++;
            } else {
                state.loadingCounter--;
            }
            state.isLoading = newLoadingStage;
        }
    },
    modules: {
        director,
        session,
        stage,
        stageTarget,
        target,
        tutorialTarget,
        tutorialStep,
        signalR,
        attendSession,
        orchestrateSession,
    },
    plugins: [createSignalRPlugin()]
});